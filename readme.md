# Formation Control for Unicycle Robots - *Distributed Robotics*

![formation scheme](https://gitlab.com/maik93/formation_proj/raw/master/cover_image.png)

Bi-dimensional Formation Control for unicycles is studied and developed in two approaches: Consensus Formation and Leader-Followers.

Robust control lows developed in Matlab-Simulink (see `readme.md` in the [appropriate folder](https://gitlab.com/maik93/formation_proj/tree/master/matlab) for further details about the code)

2D simulation ambient and final implementation of all control lows realized in R.O.S. (all nodes are written in C++).

Delivery date: May 16, 2018

## Presentation slides
Slides used in support of the exposition during the exam can be found [here](https://gitlab.com/maik93/formation_proj/raw/master/presentazione.pptx).