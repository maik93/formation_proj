# HOW TO EXECUTE THIS PACKAGE

- open `pursuit_unicycle_fbl.slx` in Simulink and execute it
- visualize output animation typing `animate(x_ref, y_ref, x, y, tout)`