% Create an animation from positions arrays (compiled by symulink file)

function animate(x_ref, y_ref, x, y, tout)

	figure('name', 'Resulting animation');
	hold on
	axis equal

	% we don't print each sample, only that one that correspond to equidistant tout
	choosen_dt = 1/5;
	choosen_fps = 1/30;
	next_t = 0;

	n_samples = length(x);
	for i = 1:n_samples
		if tout(i) >= next_t % update next_t and print this frame
			next_t = next_t + choosen_dt;

			% plot the path since here
			l1 = plot(x_ref(1:i), y_ref(1:i), 'color', 'black', 'LineWidth', 1, 'LineStyle', '-.');
			l2 = plot(x(1:i), y(1:i), 'color', rgb('Purple'), 'LineWidth', 1.5, 'LineStyle', '-');

			% plot actual positions
			p1 = scatter(x_ref(i), y_ref(i), 'black');
			p2 = scatter(x(i), y(i), 50, rgb('Purple'), 'filled');

			pause(choosen_fps);

			% if it's not the last frame remove old points and lines
			if i ~= n_samples
				delete(l1);
				delete(l2);
				delete(p1);
				delete(p2);
			end
		end
	end
end

% you can check what choosen_dt set from this plot:
%
% for i=2:length(tout)
% 	dt(i-1) = tout(i) - tout(i-1);
% end
% plot(dt)