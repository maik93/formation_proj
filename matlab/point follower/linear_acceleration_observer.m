% LINEAR_ACCELERATION_OBSERVER
%   returns the linear acceleration on the x and y axis.
%   It is used to produce the control input of the feedback
%   linearized system.

function [out] = linear_acceleration_observer(in)
	v     = in(1);
	omega = in(2);
	theta = in(3);
	vdot  = in(4);

	% xdd = -v*omega*sin(theta);
	% ydd = v*omega*cos(theta);

	xdd = vdot*cos(theta) - v*omega*sin(theta);
	ydd = vdot*sin(theta) + v*omega*cos(theta);

	out = [xdd, ydd];
end