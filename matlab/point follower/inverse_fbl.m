% INVERSE_FBL
%   This function transforms the feedback-linearization generalized
%   controls (ni1 and ni2) in the real control inputs: linear acceleration
%   and angular velocity (vdot and omega)

function [out] = inverse_fbl(in)
	% feedback linearization inputs
	ni1   = in(1);
	ni2   = in(2);
	theta = in(3);
	v     = in(4);

	% transform matrix:
	C = [cos(theta),   sin(theta);...
		-sin(theta)/v, cos(theta)/v];
	% IMPORTANT: the matrix is singular if v = 0

	ni = [ni1; ni2];

	% real input
	u = C*ni;

	vdot  = u(1);
	omega = u(2);

	out = [vdot, omega];
end