function [out] = grad_psi(z)
%PSI is the repulsive artificial potential field thought to avoid obstacle 
% other agents.
% it is used by "repulsive_f2" function for the companion avoidance

alfa = atan2(z(2), z(1));

% intensity of the potential gradient
grad_psi = -(1/20) / (norm(z)-1)^2;

% components of the gradient 
gfx = grad_psi * cos(alfa);
gfy = grad_psi * sin(alfa);

out = [gfx, gfy];

end

