% Create an animation from positions and references arrays (compiled by symulink file)

function animate(x_ref, y_ref, xl, yl, xf1, yf1, xf2, yf2, tout)

	figure('name', 'Resulting animation');
	hold on
	axis equal

	n = length(xl);
    
    % we don't print each sample, only that one that correspond to equidistant tout
	choosen_fps = 1/20;
	next_t = 0;
    
    for i = 1:n
        if tout(i) >= next_t % update next_t and print this frame
            next_t = next_t + .10;
            % plot the path since here
            l1 = plot(x_ref(1:i), y_ref(1:i), 'color', 'black', 'LineWidth', 1, 'LineStyle', '-.');
            l2 = plot(xl(1:i), yl(1:i), 'color', rgb('Purple'), 'LineWidth', 1.5, 'LineStyle', '-');
            l3 = plot(xf1(1:i), yf1(1:i), 'color', rgb('Amethyst'), 'LineWidth', 1.5, 'LineStyle', '--');
            l4 = plot(xf2(1:i), yf2(1:i), 'color', rgb('Amethyst'), 'LineWidth', 1.5, 'LineStyle', '--');
            
            % plot actual positions
            p1 = scatter(x_ref(i), y_ref(i), 'black');
            p2 = scatter(xl(i), yl(i), 50, rgb('Purple'), 'filled');
            p3 = scatter(xf1(i), yf1(i), 50, rgb('Amethyst'), 'filled');
            p4 = scatter(xf2(i), yf2(i), 50, rgb('Amethyst'), 'filled');
            
            pause(choosen_fps); % 50 fps
            
            % if it's not the last frame remove old points and lines
            if i ~= n
                delete(l1);
                delete(l2);
                delete(l3);
                delete(l4);
                delete(p1);
                delete(p2);
                delete(p3);
                delete(p4);
            end
        end
    end