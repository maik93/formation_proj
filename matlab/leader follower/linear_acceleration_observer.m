function [ out ] = linear_acceleration_observer( in )
% LINEAR_ACCELERATION_OBSERVER returns the linear acceleration on the x and
% y axis. It is used to produce the control input of the feedback
% linearized system.

v = in(1);
omega = in(2);
theta = in(3);

xdd = -v*omega*sin(theta);
ydd = v*omega*cos(theta);

out = [xdd, ydd];

end

