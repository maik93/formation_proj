function [out] = obj_avoidance_pro(in)
%OBJ_AVOIDANCE implements a test object artificial potencial to allow the
%agents safety
%   "in" is the position of the agent with respect to the fixed frame

% the object is a circle of radii equal to 2 and the center in (-10,10)
x = in(1);
y = in(2);

a = 3;
b = 2;
cx = -10 + (6/sqrt(2));
cy =  10;

f = 1*(sqrt(((x-cx)/a).^2 + ((y-cy)/b).^2) - 1).^(-3);

sigma = f/(10 + f);

out = 1-sigma;

end

