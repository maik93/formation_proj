function [ out ] = fbl_controller( in )
%GBL_CONTROLLER 
%   it implements the controller for the feedback linearized unicycle

% controller gains
k0 = 50;
k1 = 50;

% feedforward control input (linear accelerations)
xdd = in(5);
ydd = in(6);

% errors (feedback control input)
csi1 = in(1);  % x_ref - x
csi2 = in(2);  % xd_ref - xdot
csi3 = in(3);  % y_ref - y 
csi4 = in(4);  % ydot_ref - ydot 

% feedback linearized control inputs
ni1 = k0*csi1 + k1*csi3 + xdd;
ni2 = k0*csi2 + k1*csi4 + ydd;

out = [ni1, ni2];

end

