function [out] = Rlf_vel(in)
%Rfl_vel compute the derivative of Rlf, that is the rotation matrix between
%leader to follower frame.

theta = in(1) + pi/2;    % pi/2 � l'offset della formazione
omega = in(2);
z_ref = [in(3); in(4)];  % this is a happy 2 dimension vector

% Rotation matrix

R = [cos(theta), -sin(theta);...
     sin(theta),  cos(theta)];

% skw angular velocity matrix 

Om = [0,     -omega;...
      omega,      0];
  
% output: z_ref derivative

out = R * Om * z_ref;

end

