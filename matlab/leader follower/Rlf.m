function [out] = Rlf(in)
%RLF rotates a vector expressed with respect to the leader's frame to the
%fixed frame

theta = in(1) + pi/2;    % pi/2 � l'offset della formazione
z_ref = [in(2); in(3)];  % this is a vector of dimension two

% Rotation matrix 

R = [cos(theta), -sin(theta);...
     sin(theta),  cos(theta)];
 
% z_ref with respect to the fixed reference frame

out = R * z_ref;

end

