# HOW TO EXECUTE THIS PACKAGE

- open `leader_follower.slx` in Simulink and execute it
- visualize output animation typing `animate(x_ref, y_ref, x_l, y_l, x_f1, y_f1, x_f2, y_f2, tout)`