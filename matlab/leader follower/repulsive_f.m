function [out] = repulsive_f(in)
%REPULSIVE_F computes the repulsive force, which guarantees the obstacle
%avoidance
% it is used for the companion avoidance

distl = [in(1), in(2)];
distf = [in(3), in(4)];

force = - grad_psi(distl) - grad_psi(distf);

out = force;

end

