function [out] = inverse_fbl(in)
%INVERSE_FBL:
%   This function transforms the generalized feedback linearization
%   controls (ni1 and ni2) in the actual control input: linear acceleration
%   and angular velocity (vdot and omega)

% feedback linearization inputs
ni1   = in(1);
ni2   = in(2);
theta = in(3);
v     = in(4);

% transform matrix: 
% important -> the matrix is singular if v = 0; 

C = [cos(theta),   sin(theta);...
    -sin(theta)/v, cos(theta)/v];

ni = [ni1; ni2];

% actual input
u = C*ni;

vdot  = u(1);
omega = u(2);

out = [vdot, omega];
end

