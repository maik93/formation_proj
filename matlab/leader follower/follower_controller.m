function [out] = follower_controller(in)
%FOLLOWER_CONTROLLER computes the control inputs for the unicycle followers

e  = [in(1); in(2)];
ed = [in(3); in(4)];

% Gain of the controller
kp = 30;
kv = 30;

% controller 
u = kp*e + kv*ed;

out = u;

end

