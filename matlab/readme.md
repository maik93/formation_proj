# HOW IT'S ORGANIZED THIS DIRECTORY

- `path follower` and `point follower` covers the basic aspects of unicycle controls through feedback linearization.

- `consensus formation` utilizes point follower controls joined with simple consensus algorithms to realize a formation behavior where no agent is the leader, but each one contribute to group parameters.

- `leader follower` utilizes a path follower control to guide a leader, followed by agents with point follower controllers.

### Notes:
- in each directory there's an `animate` function for visualization and a tiny rgb library.