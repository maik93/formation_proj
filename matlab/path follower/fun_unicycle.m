% Unicycle model for Simulink. Inputs are [v, omega, theta] and outputs [xp, yp, thetap]

function [out] = fun_unicycle(inputs)

	v = inputs(1);
	omega = inputs(2);
	theta = inputs(3);

	xp = v*cos(theta);
	yp = v*sin(theta);
	thetap = omega;

	out = [xp, yp, thetap];
end