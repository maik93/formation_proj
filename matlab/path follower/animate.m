% Create an animation from positions matrix (compiled by symulink file)

function animate(pos_matrix, tout)

	figure('name', 'Resulting animation');
	hold on
	axis equal

	matrix_dimensions = size(pos_matrix);
	n_samples = matrix_dimensions(1);
	n_agents = matrix_dimensions(2);

	% we don't print each sample, only that one that correspond to equidistant tout
	choosen_fps = 1/50;
	next_t = 0;

	for i = 1:n_samples
		if tout(i) >= next_t % update next_t and print this frame
			next_t = next_t + choosen_fps;

			% plot paths
			l2 = plot(reshape(pos_matrix(1:i, 1), [i,1]), reshape(pos_matrix(1:i, 2), [i,1]), ...
					'color', rgb('Purple'), 'LineWidth', 1.5, 'LineStyle', '-');

			% plot actual positions
			p2 = scatter(pos_matrix(i, 1), pos_matrix(i, 2), 50, rgb('Purple'), 'filled');

			pause(1/50); % 50 fps

			% if it's not the last frame remove old points and lines
			if i ~= n_samples
				for j = 1:n_agents
					delete(l2);
					delete(p2);
				end
			end
		end
	end
end

% you can check what choosen_dt set from this plot:
%
% for i=2:length(tout)
% 	dt(i-1) = tout(i) - tout(i-1);
% end
% plot(dt)