function [out] = fun_feed_lin(inputs)

	v = inputs(1);
	vd = inputs(2);
	y = inputs(3);
	yd = inputs(4);
	theta = inputs(5);
	kp = inputs(6);
	kd = inputs(7);

	ni = - kp*y - kd*yd;
	omega = (ni - vd*sin(theta)) / (v*cos(theta));

	out = omega

end