# SIMULINK FILES IN THIS PACKAGE
- `track_unicycle_feed_lin.slx`: horizontal track follower through feedback linearization
- `track_unicycle_feed_lin_directioned.slx`: generalization of above in any desired direction (still rectilinear tracks)

## HOW TO EXECUTE THEM

- open desired Simulink file and execute it
- visualize output animation typing `animate(pos_matrix, tout)`