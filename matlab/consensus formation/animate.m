% Create an animation from positions and references matrixes (compiled by symulink file)

function animate(ref_matrix, pos_matrix, tout)

	fig = figure('name', 'Resulting animation');
	hold on
	axis equal

	matrix_dimensions = size(ref_matrix);
	n_samples = matrix_dimensions(3);
	n_agents = matrix_dimensions(2);

	% we don't print each sample, only that one that correspond to equidistant tout
	choosen_fps = 1/10;
	next_t = 0;

	for i = 1:n_samples
		if tout(i) >= next_t % update next_t and print this frame
			next_t = next_t + choosen_fps;

			for j = 1:n_agents
				% plot paths
				l1(j) = plot(reshape(ref_matrix(1,j,1:i), [i,1]), reshape(ref_matrix(2,j,1:i), [i,1]), ...
						'color', 'black', 'LineWidth', 1, 'LineStyle', '-.');
				l2(j) = plot(reshape(pos_matrix(1,j,1:i), [i,1]), reshape(pos_matrix(2,j,1:i), [i,1]), ...
						'color', rgb('Purple'), 'LineWidth', 1.5, 'LineStyle', '-');

				% plot actual positions
				p1(j) = scatter(ref_matrix(1, j, i), ref_matrix(2, j, i), 'black');
				p2(j) = scatter(pos_matrix(1, j, i), pos_matrix(2, j, i), 50, rgb('Purple'), 'filled');
			end

			pause(1/30); % 30 fps

			% saveas(fig, sprintf('img/fig_%d.png', i));

			% if it's not the last frame remove old points and lines
			if i ~= n_samples && next_t < tout(n_samples)
				for j = 1:n_agents
					delete(l1(j));
					delete(l2(j));
					delete(p1(j));
					delete(p2(j));
				end
			end
		end
	end
end

% you can check what choosen_dt set from this plot:
%
% for i=2:length(tout)
% 	dt(i-1) = tout(i) - tout(i-1);
% end
% plot(dt)