
% Geometric configuration (a.k.a. Xi), horizontal left-to-right formation direction
C = [1 0;
     0 1;
     0 -1]'; % [z1, z2, z3] column vectors

% want to plot it?
% figure
% hold on
% for i=1:length(C)
% 	scatter(C(1,i), C(2,i))
% end

n_agents = length(C);

% formation variables initial values
x_tilde = [0; 5; 0]; % equals to initial agents positions
y_tilde = [20; 10; 0];
v_tilde = rand(n_agents, 1) + 9; % formation's velocity
theta_tilde = 2*pi * rand(n_agents, 1); % formation's direction

% makes Agent 3 start horizontal (so if it would decide formation's direction it would be horizontal)
theta_tilde(3) = 0;