# HOW TO EXECUTE THIS PACKAGE

- initialize variables by calling `init` in matlab console
- open `consensus_formation.slx` in Simulink and execute it
- visualize output animation typing `animate(ref_matrix, pos_matrix, tout)`