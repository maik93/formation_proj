#include <ros/ros.h>
#include <ros/package.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Vel.h>
#include <uni_msgs/Ctrl.h>
#include <fstream>

#include "simulator/config.h"

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

auto pi = 3.14142f;

class Unicycle {
public:
    uni_msgs::Pos pose;
    uni_msgs::Vel vel{};

    Unicycle(string ID) {

        int number_line = stoi(ID) + 1; // the first line is a comment

        /// loading initial values from text file ///
        string path = ros::package::getPath("unicycle");
        ifstream text(path + "/res/initial_values.txt");
        std::string line;
        float rand, x, y, theta;

        // go  to the proper line of the text file
        for (int i = 1; i <= number_line; i++) std::getline(text, line);
        text >> rand >> x >> y >> theta;

        // use random initial values or those specified in "res/initial_values.txt"
        if (rand) {
            pose.x = getRand(sim::WINDOW_WIDTH / 2.0f, 100);
            pose.y = getRand(sim::WINDOW_HEIGHT / 2.0f, 100);
            pose.theta = getRand(2 * pi);
        } else {
            pose.x = x;
            pose.y = y;
            pose.theta = theta;
        }
        text.close();
    }

    // TODO: implement this in a new package containing libraries and definitions (e.g. LOOP_RATE and pi)
    float getRand(float max, float min = 0) {
        random_device rd; // non-deterministic 32-bit seed
        uniform_real_distribution<float> dist(min, max);
        return dist(rd);
    }

    // propagate control inputs (v, omega) through unicycle's kinematic equations
    void feedForward(const uni_msgs::Ctrl &ctrl) {
        float dt;

        dt = 1.0f / LOOP_RATE;

        vel.xd = ctrl.v * cos(pose.theta);
        vel.yd = ctrl.v * sin(pose.theta);
        vel.thetad = ctrl.omega;

        pose.x += vel.xd * dt;
        pose.y += vel.yd * dt;
        pose.theta += vel.thetad * dt;
    }
};

int main(int argc, char **argv) {
    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me unicycle ID!" << endl;
        return -1;
    }
    string uni_ID = argv[1];

    ros::init(argc, argv, "unicycle_node"); // Initiate new ROS node named "unicycle_node"

    ros::NodeHandle node;
    ros::Publisher pos_pub = node.advertise<uni_msgs::Pos>("unicycle_pose", 1);
    ros::Publisher vel_pub = node.advertise<uni_msgs::Vel>("unicycle_vel", 1);

    ros::Rate loop_rate(LOOP_RATE);

    Unicycle unicycle(uni_ID);
    ros::Subscriber ctrl_sub = node.subscribe("ctrl_input", 1, &Unicycle::feedForward, &unicycle);

    ROS_INFO("Unicycle %s online", uni_ID.c_str());

    while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C

        pos_pub.publish(unicycle.pose);
        vel_pub.publish(unicycle.vel);

        ros::spinOnce();   // Need to call this function often to allow ROS to process incoming messages
        loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
    }
    return 0;
}