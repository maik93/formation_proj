#include <ros/ros.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Vel.h>
#include <uni_msgs/Acc.h>
#include <uni_msgs/Ctrl.h>
#include <uni_msgs/Sensor.h>
#include <simulator/config.h>

#include "unicycle/parameters.h"

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

float prev_angle = 0;

float saturate(float val, float max) {
    float min = -max;
    if (val >= max)
        return max;
    else if (val <= min)
        return min;
    else
        return val;
}

class sum_and_saturate {
public:

    uni_msgs::Pos uni_pos{};
    uni_msgs::Vel uni_vel{};
    uni_msgs::Acc ref_ni{}, obs_f{};
    uni_msgs::Ctrl u{};

    uni_msgs::Sensor sensor{};

    void setPose(const uni_msgs::Pos pos) {
        uni_pos.x = pos.x;
        uni_pos.y = pos.y;
        uni_pos.theta = pos.theta;
    }

    void setVel(const uni_msgs::Vel vel) {
        uni_vel.xd = vel.xd;
        uni_vel.yd = vel.yd;
        uni_vel.thetad = vel.thetad;
    }

    void setRefCtrl(const uni_msgs::Acc ni) {
        ref_ni.xdd = ni.xdd;
        ref_ni.ydd = ni.ydd;
    }

    void setObsCtrl(const uni_msgs::Acc force) {
        obs_f.xdd = force.xdd;
        obs_f.ydd = force.ydd;
    }

    void sum_control() {

        uni_msgs::Acc acc;
        float vd, v;
        float dt;

        dt = 1.0f / LOOP_RATE;
        v = (float) sqrt(pow(uni_vel.xd, 2) + pow(uni_vel.yd, 2)); // velocity of the unicycle

        // sum all the controller input
        acc.xdd = ref_ni.xdd + obs_f.xdd;
        acc.ydd = ref_ni.ydd + obs_f.ydd;

        vd = cos(uni_pos.theta) * acc.xdd + sin(uni_pos.theta) * acc.ydd;
        vd = saturate(vd, sat::VD_MAX);

        // avoid to produce infinity control output
        float corrected_v = (v == 0) ? .001f : v;
        u.omega = ((-sin(uni_pos.theta) * acc.xdd) + (cos(uni_pos.theta) * acc.ydd)) / corrected_v;

        u.v += vd * dt;

        // pre-saturate, apply consensus obstacle avoidance, saturate again
        u.v = saturate(u.v, sat::V_MAX);
        u.omega = saturate(u.omega, sat::OMEGA_MAX);
        /// Consensus obstacle avoidance /// TODO: put in a new controller
        float w_avoid_obstacle = 0.0f;
        const int K_WD = 10, K_V_MAX = 1, K_W_MAX = 40, D_MAX = sensor::R_MAX; // TODO: put in library
        const float MOD_THETA_REF = 3.14142f / 180.0f * 7.5f; // 7.5deg
        if (sensor.detected) {
            // look for the minor angle and its sign
            float min_angle = abs(sensor.left_alpha) < abs(sensor.right_alpha) ?
                              sensor.left_alpha : sensor.right_alpha;
            float sign_min_angle = min_angle != 0 ? min_angle / abs(min_angle) : 0;

            // derivative of min_angle
            float min_angle_d = (min_angle - prev_angle) / LOOP_RATE;

            // linear K_W that increment with proximity, from (D_MAX, 0) to (0, K_W_MAX)
            float K_W = K_W_MAX * (1 - sensor.min_distance / D_MAX);

            // theta_ref to converge is in the opposite side than that one with minor absolute value
            float theta_ref, K_V;
            if (sensor.left_alpha * sensor.right_alpha < 0) { // if actual direction will hit the obstacle
                theta_ref = -sign_min_angle * MOD_THETA_REF;
                K_V = K_W / K_W_MAX * K_V_MAX;
            } else if (abs(min_angle) < MOD_THETA_REF) { // or if it's too close to front direction
                theta_ref = sign_min_angle * MOD_THETA_REF;
                K_V = 2 * K_W / K_W_MAX * K_V_MAX; // double inverse velocity than in previous case
            } else { // there's no collision, so don't produce controls
                theta_ref = min_angle;
                min_angle_d = 0;
                K_V = 0;
            }
            // PD control over theta angle and a step backward through linear velocity
            w_avoid_obstacle = K_W * (min_angle - theta_ref) + K_WD * min_angle_d;
            u.v -= K_V;

            prev_angle = min_angle;
        }
        u.omega += w_avoid_obstacle;

        u.v = saturate(u.v, sat::V_MAX);
        u.omega = saturate(u.omega, sat::OMEGA_MAX);
    }

    void sensorCallback(const uni_msgs::Sensor::ConstPtr &msg) {
        sensor.detected = msg->detected;
        sensor.right_alpha = msg->right_alpha;
        sensor.left_alpha = msg->left_alpha;
    }
};

int main(int argc, char **argv) {

    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me unicycle ID!" << endl;
        return -1;
    }
    string uni_ID = argv[1];

    ros::init(argc, argv, "sum_and_saturate");

    ros::NodeHandle node;
    ros::Publisher ctrl = node.advertise<uni_msgs::Ctrl>("ctrl_input", 1);

    sum_and_saturate sum_sat;
    ros::Subscriber pos_sub = node.subscribe("unicycle_pose", 1, &sum_and_saturate::setPose,
                                             &sum_sat);
    ros::Subscriber vel_sub = node.subscribe("unicycle_vel", 1, &sum_and_saturate::setVel,
                                             &sum_sat);
    ros::Subscriber ref_sub = node.subscribe("ctrl_ref", 1, &sum_and_saturate::setRefCtrl,
                                             &sum_sat);
    ros::Subscriber obs_sub = node.subscribe("avoidance", 1, &sum_and_saturate::setObsCtrl,
                                             &sum_sat);
    ros::Rate loop_rate(LOOP_RATE);

    ros::Subscriber sensor_sub = node.subscribe("sensor", 1, &sum_and_saturate::sensorCallback,
                                                &sum_sat);

    ROS_INFO("Sum_and_saturate unicycle %s online", uni_ID.c_str());

    while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C

        sum_sat.sum_control();
        ctrl.publish(sum_sat.u);

        ros::spinOnce();   // Need to call this function often to allow ROS to process incoming messages
        loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
    }
    return 0;
}