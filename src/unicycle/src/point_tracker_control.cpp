#include <ros/ros.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Vel.h>
#include <uni_msgs/Acc.h>
#include <uni_msgs/Ctrl.h>

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

class PointTrackerControl {
public:
    float kp, kv; // PD coefficients

    // input variables: reference and actual position, velocity and acceleration
    uni_msgs::Pos ref_pos{}, act_pos{};
    uni_msgs::Vel ref_vel{}, act_vel{};
    uni_msgs::Acc ref_acc{};
    uni_msgs::Acc ni;

    // control variables (Controller -> Unicycle)
    uni_msgs::Ctrl u;

    PointTrackerControl() {
        kp = 40;
        kv = 15;
        u.v = 0;
        u.omega = 0;
    }

    void setRefPos(const uni_msgs::Pos &pos) {
        ref_pos.x = pos.x;
        ref_pos.y = pos.y;
    }

    void setRefVel(const uni_msgs::Vel &vel) {
        ref_vel.xd = vel.xd;
        ref_vel.yd = vel.yd;
    }

    void setRefAcc(const uni_msgs::Acc &acc) {
        ref_acc.xdd = acc.xdd;
        ref_acc.ydd = acc.ydd;
    }

    void setActualPos(const uni_msgs::Pos &pos) {
        act_pos.x = pos.x;
        act_pos.y = pos.y;
        act_pos.theta = pos.theta;
    }

    void setActualVel(const uni_msgs::Vel &vel) {
        act_vel.xd = vel.xd;
        act_vel.yd = vel.yd;
    }

    // evaluate controller output (v, omega)
    void evaluateControl() {
        float v;
        float vd;
        float dt;

        // compute the controller output for the double integrator model
        ni.xdd = kp * (ref_pos.x - act_pos.x) + kv * (ref_vel.xd - act_vel.xd) + ref_acc.xdd;
        ni.ydd = kp * (ref_pos.y - act_pos.y) + kv * (ref_vel.yd - act_vel.yd) + ref_acc.ydd;
    }
};

int main(int argc, char **argv) {

    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me unicycle control ID!" << endl;
        return -1;
    }
    string uni_ID = argv[1];

    ros::init(argc, argv, "controller_node"); // Initiate new ROS node named "controller_node"

    ros::NodeHandle node;
    ros::Publisher ctrl_pub = node.advertise<uni_msgs::Acc>("ctrl_ref", 1);

    PointTrackerControl ctrl_obj;
    ros::Subscriber pos_ref = node.subscribe("pos_ref", 1, &PointTrackerControl::setRefPos,
                                             &ctrl_obj);
    ros::Subscriber vel_ref = node.subscribe("vel_ref", 1, &PointTrackerControl::setRefVel,
                                             &ctrl_obj);
    ros::Subscriber acc_ref = node.subscribe("acc_ref", 1, &PointTrackerControl::setRefAcc,
                                             &ctrl_obj);
    ros::Subscriber pos_uni = node.subscribe("unicycle_pose", 1,
                                             &PointTrackerControl::setActualPos,
                                             &ctrl_obj);
    ros::Subscriber vel_uni = node.subscribe("unicycle_vel", 1, &PointTrackerControl::setActualVel,
                                             &ctrl_obj);

    ros::Rate loop_rate(LOOP_RATE);

    ROS_INFO("Controller %s online", uni_ID.c_str());

    while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C

        ctrl_obj.evaluateControl();
        ctrl_pub.publish(ctrl_obj.ni);

        ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
        loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
    }
    return 0;
}