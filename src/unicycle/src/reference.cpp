#include <ros/ros.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Vel.h>
#include <uni_msgs/Acc.h>

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

auto pi = 3.14142f;

// TODO: inherit class in "unicycle/src/unicycle.cpp"
class Reference {
public:
    uni_msgs::Pos pose;
    uni_msgs::Vel vel{};
    uni_msgs::Acc acc{};
    float v, omega; // control input
//    float vd, prev_v; // in case v is not constant, its derivative is needed

    Reference() {
        // TODO: init from file or from argv?
        pose.x = 0;
        pose.y = 0;
        pose.theta = pi / 4;
        v = 20;
//        vd = 0;
        omega = 0;
    }

    // execute one time-step
    void tick() {
        float dt;

        dt = 1.0f / LOOP_RATE;

        vel.xd = v * cos(pose.theta);
        vel.yd = v * sin(pose.theta);
        vel.thetad = omega;

        pose.x += vel.xd * dt;
        pose.y += vel.yd * dt;
        pose.theta += vel.thetad * dt;

//        vd = (v - prev_v) / dt;
    }

    // evaluate the acceleration
    void evalAcc() {
//        acc.xdd = vd * cos(pose.theta) - v * omega * sin(pose.theta);
//        acc.ydd = vd * sin(pose.theta) + v * omega * cos(pose.theta);
        acc.xdd = -v * omega * sin(pose.theta);
        acc.ydd = v * omega * cos(pose.theta);
    }
};

int main(int argc, char **argv) {
    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me reference ID!" << endl;
        return -1;
    }
    string ref_ID = argv[1];

    ros::init(argc, argv, "reference_node"); // Initiate new ROS node named "reference_node"

    ros::NodeHandle node;
    // This node publishes: position, velocity and acceleration
    ros::Publisher pos_pub = node.advertise<uni_msgs::Pos>("agent_" + ref_ID + "/pos_ref", 1);
    ros::Publisher vel_pub = node.advertise<uni_msgs::Vel>("agent_" + ref_ID + "/vel_ref", 1);
    ros::Publisher acc_pub = node.advertise<uni_msgs::Acc>("agent_" + ref_ID + "/acc_ref", 1);

    ros::Rate loop_rate(LOOP_RATE);

    Reference reference;

    ROS_INFO("Reference %s online", ref_ID.c_str());

    while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C

        // evaluate the kinematic and acceleration of the reference
        reference.tick();
        reference.evalAcc();

        pos_pub.publish(reference.pose);
        vel_pub.publish(reference.vel);
        acc_pub.publish(reference.acc);

        ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
        loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
    }
    return 0;
}