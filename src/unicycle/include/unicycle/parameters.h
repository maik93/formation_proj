#ifndef UNICYCLE_PARAMETERS_H
#define UNICYCLE_PARAMETERS_H

namespace sat {
    const float V_MAX = 40;     // max value of the linear velocity of the unicycle
    const float VD_MAX = 120;   // max value of the linear acceleration of the unicycle
    const float OMEGA_MAX = 2;  // max value of the angular velocity of the unicycle
}

//auto pi = 3,14159f;

#endif //UNICYCLE_PARAMETERS_H
