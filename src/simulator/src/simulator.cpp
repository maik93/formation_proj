#include <SFML/Graphics.hpp>
#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/Float64.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Sensor.h>

#include "simulator/config.h"

using namespace sf;

const bool PACMAN_EFFECT = true; // exit from one side will enter in the opposite

const bool DRAW_SENSORS = true;
bool SENSOR_ENABLED = false;

auto pi = 3.14142f, r2d = 180 / pi;

// a window image containing only obstacles
RenderTexture screen_obstacles;

class Arrow {
public:
    int agent_ID{};
    bool is_alive = false;
    Sprite path_lines, arrow_sprite; // what to print on screen
    Color color;

private:
    uni_msgs::Pos pose; // private because with strange ref system!
    float x_prev{}, y_prev{};
    RenderTexture path_lines_texture; // where to draw (linked to path_lines)
    ros::Subscriber pose_sub;
    ros::Publisher sensor_pub;

public:
    Arrow() = default;

    Arrow(int id, Color col, Texture &texture, ros::NodeHandle *node) {
        agent_ID = id;
        color = col;

        /// ROS topics ///
        // progressive names: e.g. "agent_<j>/unicycle_pose"
        String topic_name = "agent_" + std::to_string(agent_ID) + "/unicycle_pose";
        pose_sub = node->subscribe(topic_name, 1, &Arrow::poseCallback, this);

        topic_name = "agent_" + std::to_string(agent_ID) + "/sensor";
        sensor_pub = node->advertise<uni_msgs::Sensor>(topic_name, 1);

        /// Graphics ///
        // initialize position out of screen
        pose.x = -50;
        pose.y = -50;

        // initialize textures and sprites
        path_lines_texture.create(sim::WINDOW_WIDTH, sim::WINDOW_HEIGHT);
        path_lines_texture.setSmooth(true);
        path_lines.setTexture(path_lines_texture.getTexture());

        arrow_sprite.setTexture(texture);
        auto dim = texture.getSize();
        arrow_sprite.setScale(.75, .75);
//        arrow_sprite.setOrigin(dim.x * .75f / 2.0f, dim.y * .75f / 2.0f);
        arrow_sprite.setOrigin(dim.x / 2.0f, dim.y / 2.0f);
        arrow_sprite.setColor(color);

        updatePathAndArrow();
    }

    void checkCorners() {
        while (pose.x * pose.y < 0 || pose.x >= sim::WINDOW_WIDTH || pose.y >= sim::WINDOW_HEIGHT) {
            pose.x = limitValue(pose.x, sim::WINDOW_WIDTH);
            pose.y = limitValue(pose.y, sim::WINDOW_HEIGHT);
        }
    }

    void updatePathAndArrow() {
        if (abs(static_cast<int>(pose.x - x_prev)) > sim::WINDOW_WIDTH - 10) x_prev = pose.x;
        if (abs(static_cast<int>(pose.y - y_prev)) > sim::WINDOW_HEIGHT - 10) y_prev = pose.y;

        Vertex line[] = {Vertex(Vector2f(pose.x, pose.y), color),
                         Vertex(sf::Vector2f(x_prev, y_prev), color)};
        path_lines_texture.draw(line, 2, Lines); // if u want more thicker lines look for Quads
        path_lines_texture.display(); // "compile" the texture (mandatory)

        arrow_sprite.setPosition(pose.x, pose.y);
        arrow_sprite.setRotation(pose.theta * r2d); // degree!
    }

    // x is the same between graphic system and classic reference system
    float getX() const { return pose.x; }

    void setX(float x) { Arrow::pose.x = x; }

    // graphic y starts from top
    float getY() const { return sim::WINDOW_HEIGHT - pose.y; }

    void setY(float y) { Arrow::pose.y = sim::WINDOW_HEIGHT - y; }

    // graphic theta is counterclockwise
//    float getTheta() const { return -pose.theta; }

    void setTheta(float theta) { Arrow::pose.theta = -theta; }

//    float getX_prev() const { return x_prev; }

    void setX_prev(float x_prev) { Arrow::x_prev = x_prev; }

//    float getY_prev() const { return sim::WINDOW_HEIGHT - y_prev; }

    void setY_prev(float y_prev) { Arrow::y_prev = sim::WINDOW_HEIGHT - y_prev; }

//    Vector3f getColor() { return Vector3f(color.r, color.g, color.b); }

    void poseCallback(const uni_msgs::Pos::ConstPtr &msg) {
        if (!is_alive) { // it's the first message for this arrow
            is_alive = true;
            setX_prev(msg->x);
            setY_prev(msg->y);
        } else {
            setX_prev(getX());
            setY_prev(getY());
        }
        setX(msg->x);
        setY(msg->y);
        setTheta(msg->theta);
    }

    // Makes a scan in direction imposed by the passed angle (graphic ref system, in rads).
    int sensorDistance(float alpha, const Image *screen, RenderWindow *window) {
        unsigned int x, y, dist;
        Color pixel;

        dist = sensor::R_MIN;
        do {
            x = static_cast<unsigned int>(pose.x + dist * std::cos(alpha));
            y = static_cast<unsigned int>(pose.y + dist * std::sin(alpha)); // alpha is positive clockwise
            if (x < sim::WINDOW_WIDTH && y < sim::WINDOW_HEIGHT)
                pixel = screen->getPixel(x, y);
            else pixel = Color::White;

            dist = dist + sensor::R_STEP;
        } while ((dist < sensor::R_MAX) && (pixel == Color::White));

        // draw coloured dots where there's an obstacle (debug purpose)
        if (SENSOR_ENABLED && DRAW_SENSORS && (dist < sensor::R_MAX) && (pixel != Color::White)) {
            CircleShape dot(5);
            dot.setOrigin(5, 5);
            dot.setFillColor(pixel);
            dot.setPosition(x, y);
            window->draw(dot);
        }

        if (dist >= sensor::R_MAX) return -1;
        return dist;
    }


    // publish the two angles that surround an eventual obstacle in front of the agent
    void readSensor(const Image *screen, RenderWindow *window) {
        uni_msgs::Sensor sensor;
        float right_alpha = 0, left_alpha = 0;
        bool got_first_angle = false;
        float min_dist = sensor::R_MAX;

        for (float rel_alpha = sensor::A_MIN; rel_alpha <= sensor::A_MAX; rel_alpha += sensor::A_STEP) {
            int dist = sensorDistance(rel_alpha + pose.theta, screen, window);
            if (dist != -1) {
                if (!got_first_angle) { // right angle is the first one to collect
                    got_first_angle = true;
                    left_alpha = rel_alpha;
                } else // if I already have the right angle, I store this new as the left one
                    right_alpha = rel_alpha;

                if (dist < min_dist) min_dist = dist;
            }
        }
//        std::cout << min_dist << std::endl;
        if (got_first_angle && right_alpha != 0 && left_alpha != right_alpha) {
//            std::cout << "[Agent " << agent_ID << "] Obstacle in [" << -right_alpha * 180 / pi
//                      << ", " << -left_alpha * 180 / pi << "]" << std::endl;

            // publish angle values
            sensor.detected = true;
            sensor.right_alpha = -right_alpha; // convert graphic to usual angle notation
            sensor.left_alpha = -left_alpha;
            sensor.min_distance = min_dist;
            sensor_pub.publish(sensor);
        } else {
            // publish empty message
            sensor.detected = false;
            sensor_pub.publish(sensor);
        }
    }

    void drawSensor(RenderWindow *window) {
        CircleShape outer_R(sensor::R_MAX);
        outer_R.setFillColor(Color::Transparent);
        outer_R.setOutlineThickness(1);
        outer_R.setOutlineColor(Color::Black);
        outer_R.setOrigin(sensor::R_MAX, sensor::R_MAX);
        outer_R.setPosition(pose.x, pose.y);

        CircleShape inner_R(sensor::R_MIN);
        inner_R.setFillColor(Color::Transparent);
        inner_R.setOutlineThickness(1);
        inner_R.setOutlineColor(Color::Black);
        inner_R.setOrigin(sensor::R_MIN, sensor::R_MIN);
        inner_R.setPosition(pose.x, pose.y);

        RectangleShape line_min(Vector2f(sensor::R_MAX - sensor::R_MIN, 1));
        line_min.setFillColor(Color::Black);
        line_min.setOrigin(-sensor::R_MIN, 0);
        line_min.rotate((sensor::A_MIN + pose.theta) * 180 / pi);
        line_min.setPosition(pose.x, pose.y);

        RectangleShape line_max(Vector2f(sensor::R_MAX - sensor::R_MIN, 1));
        line_max.setFillColor(Color::Black);
        line_max.setOrigin(-sensor::R_MIN, 0);
        line_max.rotate((sensor::A_MAX + pose.theta) * 180 / pi);
        line_max.setPosition(pose.x, pose.y);

        window->draw(outer_R);
        window->draw(inner_R);
        window->draw(line_min);
        window->draw(line_max);
    }

private:
    // limit a variable in the interval [0,limit)
    float limitValue(float val, float limit) {
        if (val >= limit) return val - limit;
        if (val < 0) return val + limit;
        return val;
    }
};

CircleShape draw_ellipse(int i) {
    CircleShape ellipse(1);
    ellipse.setOrigin(ellipse.getRadius(), ellipse.getRadius()); // the origin of the ellipse is set at the center
    ellipse.scale(sim::a[i], sim::b[i]);    // the circle is scaled to the desired ellipse shape
    ellipse.setFillColor(Color::Black);
    ellipse.setPosition(sim::cx[i], sim::WINDOW_HEIGHT - sim::cy[i]);
    return ellipse;
}

void draw_obstacle(RenderTexture &screen_buff) {
    for (int i = 0; i < sim::n_obs; ++i) {
        CircleShape ellipse = draw_ellipse(i);
        screen_buff.draw(ellipse);
    }
}

void draw_obstacle(RenderWindow &window) {
    for (int i = 0; i < sim::n_obs; ++i) {
        CircleShape ellipse = draw_ellipse(i);
        window.draw(ellipse);
    }
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "arrow_simulator"); // Initiate new ROS node named "arrow_simulator"
    ros::NodeHandle node;
//    ros::Rate loop_rate(FPS); // loop cycles handled by SFML

    String path = ros::package::getPath("simulator");

    // check number of input arguments
    int n_arrows;
    if (argc == 1) {
        std::cout << "Invalid call: Syntax: \"simulator <n_arrows> [--with-sensors]\". Assuming one arrow!"
                  << std::endl;
        n_arrows = 1;
    } else n_arrows = atoi(argv[1]);

    // enable distance sensors
    std::string sensor_enabler_string = "--with-sensors";
    if (argc >= 3 && !sensor_enabler_string.compare(argv[2])) {
        std::cout << "enabling sensors" << std::endl;
        SENSOR_ENABLED = true;
    } else std::cout << "sensors disabled (append \"--with-sensors\" to use them)" << std::endl;

    Arrow *arrows[n_arrows]; // now that I know the needed dimension, I create the arrow array

    /// Graphics ///
    // Window interface
    RenderWindow window(VideoMode(sim::WINDOW_WIDTH, sim::WINDOW_HEIGHT), "Arrow simulator");
    window.setPosition(Vector2i(sim::WINDOW_X, sim::WINDOW_Y)); // position from top-left corner of the desktop
    window.setFramerateLimit(sim::FPS);

    // a window image containing only obstacles
    screen_obstacles.create(sim::WINDOW_WIDTH, sim::WINDOW_HEIGHT);

    // background image
    Texture bg_texture;
    bg_texture.loadFromFile(path + "/res/background.jpg");
    Sprite background(bg_texture);
    background.setColor(sf::Color(255, 255, 255, 32)); // 7/8 transparent

    // arrows image
    Texture arrow_texture;
    arrow_texture.loadFromFile(path + "/res/arrow.png");
    arrow_texture.setSmooth(true);

    for (int j = 0; j < n_arrows; j++)
        arrows[j] = new Arrow(j, Color::Magenta, arrow_texture, &node);

    // text (problem with fonts: where I can get them in linux?)
//    Font win_font; win_font.loadFromFile(sansation.ttf");
//    Text win_text("some text", win_font, 35);

    /// ROS stuff ///
    // simulation time for other nodes
//    ros::Publisher time_pub = node.advertise<std_msgs::Float64>("sim_time", 1);
//    const double start_T = ros::Time::now().toSec();

    /// main loop ///
    while (window.isOpen() && ros::ok()) {
        Event e{};
        while (window.pollEvent(e)) {
            if (e.type == Event::Closed) // close window event
                window.close();
        }

        /// simulation time ///
//        std_msgs::Float64 current_T;
//        current_T.data = ros::Time::now().toSec() - start_T;
//        time_pub.publish(current_T); // in seconds

        /// user inputs ///
        if (Keyboard::isKeyPressed(Keyboard::Escape)) window.close(); // ESC key will close

        /// draw ///
        window.clear(Color::White);
        window.draw(background);
        draw_obstacle(window);

        if (SENSOR_ENABLED) {
            screen_obstacles.clear(Color::White);
            draw_obstacle(screen_obstacles);
        }

        for (int j = 0; j < n_arrows; j++) {
            if (PACMAN_EFFECT) arrows[j]->checkCorners();
            arrows[j]->updatePathAndArrow();
            window.draw(arrows[j]->path_lines);
            window.draw(arrows[j]->arrow_sprite);

            screen_obstacles.draw(arrows[j]->arrow_sprite);

            if (SENSOR_ENABLED && DRAW_SENSORS) arrows[j]->drawSensor(&window);
        }

        /// obstacle sensor detection ///
        if (SENSOR_ENABLED) {
            screen_obstacles.display(); // "compile" the RenderTexture
            const Image screen = screen_obstacles.getTexture().copyToImage();
            for (int j = 0; j < n_arrows; j++)
                arrows[j]->readSensor(&screen, &window);
        }

        window.display();

        ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
//        loop_rate.sleep();
    }

    return 0;
}
