#ifndef PROJECT_SIMULATOR_H
#define PROJECT_SIMULATOR_H

namespace sim {

    /// Graphical properties ///
    // window dimensions and position (relative to top-left of the screen)
    const int WINDOW_WIDTH = 1080;
    const int WINDOW_HEIGHT = 600;
    const int WINDOW_X = 20;
    const int WINDOW_Y = 5;

    // window MAXIMUM frames per second
    const int FPS = 60;

    /// Obstacles ///
    // defines the center (cx, cy) and semi-major and semi-minor axis (a, b) of the ellipse-obstacle
    const auto n_obs = 5;
    const float cx[n_obs] = {750, 250, 460, 600, 500};
    const float cy[n_obs] = {500, 430, 380, 340, 100};
    const float a[n_obs]  = {30,  20,  25,  10,  40};
    const float b[n_obs]  = {50,  10,  15,  15,  20};
}

/// sensor parameters ///
namespace sensor {
    const int R_MIN = 11; // minimum distance to scan (pixels)
    const int R_STEP = 2; // scanner increment distance
    const int R_MAX = 35; // max distance to scan
    const float A_MIN = -3.14142f / 12; // min and max scanning angle (rads)
    const float A_MAX = 3.14142f / 12;
    const float A_STEP = 3.14142f / 90; // = 2°
}

#endif //PROJECT_SIMULATOR_H
