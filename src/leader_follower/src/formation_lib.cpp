#include <ros/ros.h>
#include "leader_follower/formation_lib.h"

// evaluate the relative formation position with respect the fixed frame
std::tuple<float, float> pos_fixed_frame(float xl, float yl, float theta) {
    float xf, yf;
    xf = xl * cos(theta) - yl * sin(theta);
    yf = xl * sin(theta) + yl * cos(theta);
    return std::make_tuple(xf, yf);
}

// evaluate the derivative of the relative formation position with respect the fixed frame
std::tuple<float, float> vel_fixed_frame(float xl, float yl, float theta, float thetad) {
    float xdf, ydf;
    xdf = -thetad * yl * cos(theta) - thetad * xl * sin(theta);
    ydf = -thetad * yl * sin(theta) + thetad * xl * cos(theta);
    return std::make_tuple(xdf, ydf);
}