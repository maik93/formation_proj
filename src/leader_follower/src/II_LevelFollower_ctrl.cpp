// generate the control input for the follower based on the position
// in the formation that must be acquired

#include <ros/ros.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Vel.h>
#include <uni_msgs/Acc.h>
#include <uni_msgs/Ctrl.h>
#include <uni_msgs/FormationStatus.h>

#include "leader_follower/formation_lib.h"

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

class Control_f {
public:
    float kp, kv;       // PD coefficients
    float error1, error2;        // absolute value of the formation error
    uni_msgs::Pos e1{}, e2{};
    uni_msgs::Vel ed1{}, ed2{};
    uni_msgs::Acc ni{};

    uni_msgs::Pos lead1_pos{}, lead2_pos{}, uni_pos{}, z1_ref{}, z2_ref{}, z1_local{}, z2_local{};
    uni_msgs::Vel lead1_vel{}, lead2_vel{}, uni_vel{}, zd1_ref{}, zd2_ref{};

    uni_msgs::FormationStatus status;

    Control_f(string ID) {
        kp = 20;
        kv = 10;

        status.agent_ID = ID;
        status.in_formation = false;
    }

    void p_uni(const uni_msgs::Pos &pos) {
        uni_pos.x = pos.x;
        uni_pos.y = pos.y;
        uni_pos.theta = pos.theta;
    }

    void v_uni(const uni_msgs::Vel &vel) {
        uni_vel.xd = vel.xd;
        uni_vel.yd = vel.yd;
        uni_vel.thetad = vel.thetad;
    }

    void p_lead1(const uni_msgs::Pos &pos) {
        lead1_pos.x = pos.x;
        lead1_pos.y = pos.y;
        lead1_pos.theta = pos.theta;
    }

    void v_lead1(const uni_msgs::Vel &vel) {
        lead1_vel.xd = vel.xd;
        lead1_vel.yd = vel.yd;
        lead1_vel.thetad = vel.thetad;
    }

    void p_lead2(const uni_msgs::Pos &pos) {
        lead2_pos.x = pos.x;
        lead2_pos.y = pos.y;
        lead2_pos.theta = pos.theta;
    }

    void v_lead2(const uni_msgs::Vel &vel) {
        lead2_vel.xd = vel.xd;
        lead2_vel.yd = vel.yd;
        lead2_vel.thetad = vel.thetad;
    }

    void evaluate_reference() {
        // evaluation of the y coordinate in the local reference frame of the agent
        // to analyse the most right and most left leader and choose the right reference formation
        float yl1, yl2;

        yl1 = -sin(uni_pos.theta) * lead1_pos.x + cos(uni_pos.theta) * lead1_pos.y;
        yl2 = -sin(uni_pos.theta) * lead2_pos.x + cos(uni_pos.theta) * lead2_pos.y;

        if (yl1 > yl2) { // true if the leader 1 is the most left leader
            z1_local.x = form::x_formation[1];
            z1_local.y = form::y_formation[1];
            z2_local.x = form::x_formation[0];
            z2_local.y = form::y_formation[0];
        } else {
            z1_local.x = form::x_formation[0];
            z1_local.y = form::y_formation[0];
            z2_local.x = form::x_formation[1];
            z2_local.y = form::y_formation[1];
        }
    }

    void rel_z() {
        // evaluation of the reference in the fixed frame
        // reference with respect the FIRST LEADER
        tie(z1_ref.x, z1_ref.y) = pos_fixed_frame(z1_local.x, z1_local.y, lead1_pos.theta);
        tie(zd1_ref.xd, zd1_ref.yd) = vel_fixed_frame(z1_local.x, z1_local.y, lead1_pos.theta, lead1_vel.thetad);
        // reference with respect the SECOND LEADER
        tie(z2_ref.x, z2_ref.y) = pos_fixed_frame(z2_local.x, z2_local.y, lead2_pos.theta);
        tie(zd2_ref.xd, zd2_ref.yd) = vel_fixed_frame(z2_local.x, z2_local.y, lead2_pos.theta, lead2_vel.thetad);
    }

    void following_error() {  // evaluate the error in formation

        evaluate_reference(); // the two leader can invert position and the agent must prevent that
        rel_z();              // express the reference in fixed frame coordinates

        e1.x = lead1_pos.x - uni_pos.x + z1_ref.x;
        e1.y = lead1_pos.y - uni_pos.y + z1_ref.y;
        ed1.xd = lead1_vel.xd - uni_vel.xd + zd1_ref.xd;
        ed1.yd = lead1_vel.yd - uni_vel.yd + zd1_ref.yd;

        e2.x = lead2_pos.x - uni_pos.x + z2_ref.x;
        e2.y = lead2_pos.y - uni_pos.y + z2_ref.y;
        ed2.xd = lead2_vel.xd - uni_vel.xd + zd2_ref.xd;
        ed2.yd = lead2_vel.yd - uni_vel.yd + zd2_ref.yd;

        error1 = sqrt(pow(e1.x, 2.0f) + pow(e1.y, 2.0f));
        status.in_formation = error1 < 5 && error2 < 5;
    }

    void controller() {  // compute the control input for the double integrator model
        following_error();
        ni.xdd = kp * (e1.x + e2.x) + kv * (ed1.xd + ed2.xd);
        ni.ydd = kp * (e1.y + e2.y) + kv * (ed1.yd + ed2.yd);
    }
};

int main(int argc, char **argv) {

    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me follower control ID!" << endl;
        return -1;
    }
    string uni_ID = argv[1];

    ros::init(argc, argv, "special_follower_controller" + uni_ID); // Initiate new ROS node named "follower_controller"

    ros::NodeHandle node;
    ros::Publisher ctrl_pub = node.advertise<uni_msgs::Acc>("/agent_" + uni_ID + "/ctrl_ref", 1);
    ros::Publisher status_pub = node.advertise<uni_msgs::FormationStatus>("formation_status", 5);

    Control_f fol_ctrl(uni_ID);
    // leader's parameters subscription
    ros::Subscriber p_lead1 = node.subscribe("/agent_1/unicycle_pose", 1, &Control_f::p_lead1, &fol_ctrl);
    ros::Subscriber v_lead1 = node.subscribe("/agent_1/unicycle_vel", 1, &Control_f::v_lead1, &fol_ctrl);
    ros::Subscriber p_lead2 = node.subscribe("/agent_2/unicycle_pose", 1, &Control_f::p_lead2, &fol_ctrl);
    ros::Subscriber v_lead2 = node.subscribe("/agent_2/unicycle_vel", 1, &Control_f::v_lead2, &fol_ctrl);
    // follower's parameters subscription
    ros::Subscriber p_uni = node.subscribe("/agent_" + uni_ID + "/unicycle_pose", 1, &Control_f::p_uni, &fol_ctrl);
    ros::Subscriber v_uni = node.subscribe("/agent_" + uni_ID + "/unicycle_vel", 1, &Control_f::v_uni, &fol_ctrl);
    ros::Rate loop_rate(LOOP_RATE);

    ROS_INFO("Second Level Follower controller %s online", uni_ID.c_str());

    while (ros::ok()) {// Keep spinning loop until user presses Ctrl+C

        fol_ctrl.controller();
        ctrl_pub.publish(fol_ctrl.ni);
        status_pub.publish(fol_ctrl.status);

        ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
        loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
    }
    return 0;
}