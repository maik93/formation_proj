// generate the control input for the follower based on the position
// in the formation that must be acquired

#include <ros/ros.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Vel.h>
#include <uni_msgs/Acc.h>
#include <uni_msgs/Ctrl.h>
#include <uni_msgs/FormationStatus.h>

#include "leader_follower/formation_lib.h"

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

class Control_f {
public:
    float kp, kv;       // PD coefficients
    float error;        // absolute value of the formation error
    uni_msgs::Pos e{};
    uni_msgs::Vel ed{};
    uni_msgs::Acc ni{};

    uni_msgs::Pos lead_pos{}, uni_pos{}, z_ref{}, z_local;
    uni_msgs::Vel lead_vel{}, uni_vel{}, zd_ref{};

    uni_msgs::FormationStatus status;

    Control_f(string ID) {
        kp = 20;
        kv = 10;

        status.agent_ID = ID;
        status.in_formation = false;
    }

    void p_uni(const uni_msgs::Pos &pos) {
        uni_pos.x = pos.x;
        uni_pos.y = pos.y;
        uni_pos.theta = pos.theta;
    }

    void v_uni(const uni_msgs::Vel &vel) {
        uni_vel.xd = vel.xd;
        uni_vel.yd = vel.yd;
        uni_vel.thetad = vel.thetad;
    }

    void p_lead(const uni_msgs::Pos &pos) {
        lead_pos.x = pos.x;
        lead_pos.y = pos.y;
        lead_pos.theta = pos.theta;
    }

    void v_lead(const uni_msgs::Vel &vel) {
        lead_vel.xd = vel.xd;
        lead_vel.yd = vel.yd;
        lead_vel.thetad = vel.thetad;
    }

    void rel_z(const uni_msgs::Pos &z_local) {
        // evaluation of the reference in the fixed frame
        tie(z_ref.x, z_ref.y) = pos_fixed_frame(z_local.x, z_local.y, lead_pos.theta);
        tie(zd_ref.xd, zd_ref.yd) = vel_fixed_frame(z_local.x, z_local.y, lead_pos.theta, lead_vel.thetad);
    }

    void following_error() {  // evaluate the error in formation
        e.x = lead_pos.x - uni_pos.x + z_ref.x;
        e.y = lead_pos.y - uni_pos.y + z_ref.y;
        ed.xd = lead_vel.xd - uni_vel.xd + zd_ref.xd;
        ed.yd = lead_vel.yd - uni_vel.yd + zd_ref.yd;

        error = sqrt(pow(e.x, 2.0f) + pow(e.y, 2.0f));
        status.in_formation = error < 5;
    }

    void controller() {  // compute the control input for the double integrator model
        following_error();
        ni.xdd = kp * e.x + kv * ed.xd;
        ni.ydd = kp * e.y + kv * ed.yd;
    }
};

int main(int argc, char **argv) {

    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me follower control ID!" << endl;
        return -1;
    }
    string uni_ID = argv[1];

    ros::init(argc, argv, "follower_controller" + uni_ID); // Initiate new ROS node named "follower_controller"

    ros::NodeHandle node;
    ros::Publisher ctrl_pub = node.advertise<uni_msgs::Acc>("/agent_" + uni_ID + "/ctrl_ref", 1);
    ros::Publisher status_pub = node.advertise<uni_msgs::FormationStatus>("/formation_status", 5);

    Control_f fol_ctrl(uni_ID);
    // leader's parameters subscription
    ros::Subscriber p_lead = node.subscribe("/agent_0/unicycle_pose", 1, &Control_f::p_lead, &fol_ctrl);
    ros::Subscriber v_lead = node.subscribe("/agent_0/unicycle_vel", 1, &Control_f::v_lead, &fol_ctrl);
    // formation position subscription
    ros::Subscriber z_ref_sub = node.subscribe("/agent_" + uni_ID + "/relative_z", 1, &Control_f::rel_z, &fol_ctrl);
    // follower's parameters subscription
    ros::Subscriber p_uni = node.subscribe("/agent_" + uni_ID + "/unicycle_pose", 1, &Control_f::p_uni, &fol_ctrl);
    ros::Subscriber v_uni = node.subscribe("/agent_" + uni_ID + "/unicycle_vel", 1, &Control_f::v_uni, &fol_ctrl);
    ros::Rate loop_rate(LOOP_RATE);

    ROS_INFO("First Level Follower controller %s online", uni_ID.c_str());

    while (ros::ok()) {// Keep spinning loop until user presses Ctrl+C

        fol_ctrl.controller();
        ctrl_pub.publish(fol_ctrl.ni);
        status_pub.publish(fol_ctrl.status);

        ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
        loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
    }
    return 0;
}