#ifndef UNICYCLE_FORMATION_LIB_H
#define UNICYCLE_FORMATION_LIB_H

namespace form {
    const int n_companion = 3;           // number of companion
    float x_formation[2] = {-30, -30};   // x coordinates of the formation points
    float y_formation[2] = {30, -30};    // y coordinates of the formation points
}

/// Formation function
std::tuple<float, float> pos_fixed_frame(float xl, float yl, float theta);

std::tuple<float, float> vel_fixed_frame(float xl, float yl, float theta, float thetad);

#endif //UNICYCLE_FORMATION_LIB_H
