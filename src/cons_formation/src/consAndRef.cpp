//#include <Eigen/Core>
#include <ros/ros.h>
#include <std_msgs/Float64.h>

#include <uni_msgs/Pos.h>
#include <uni_msgs/Vel.h>
#include <uni_msgs/Acc.h>
#include <uni_msgs/ConsParam.h>
#include <uni_msgs/ConsParamArray.h>
#include <std_msgs/Int32.h>

#include "cons_formation/config.h"

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

auto pi = 3.14142f;

// current simulation time
//float current_T = -1;

// to initialize consensus parameters
uni_msgs::Pos starting_pose;
bool has_pose;

// callback for waitForPose
void getPose(const uni_msgs::Pos &msg) {
    starting_pose.x = msg.x;
    starting_pose.y = msg.y;
    starting_pose.theta = msg.theta;
    has_pose = true;
}

// blocking function that wait for a pose to initialize consensus parameters
void waitForPose(const string &agent_ID, ros::NodeHandle *node, ros::Rate *loop_rate) {
    ros::Subscriber pos_sub = node->subscribe("unicycle_pose", 1, &getPose);
    while (!has_pose) {
        ros::spinOnce();
        loop_rate->sleep();
    }
}

class Consensus {
public:
    int agent_ID;

    // relative position of this agent in the formation
    float x_rel, y_rel;

    int config_num = 0; // formation configuration number [0,1]

    // formation consensus parameters (x, y, v, theta)
    uni_msgs::ConsParam cons_param;

    // reference output position and velocity
    uni_msgs::Pos ref_pos;
    uni_msgs::Vel ref_vel, prev_vel; // prev_vel is used to compute ref_acc
    uni_msgs::Acc ref_acc;

    // initialize consensus parameters as own starting position
    explicit Consensus(int id) {
        agent_ID = id;
        x_rel = formationConfiguration[config_num][0][agent_ID];
        y_rel = formationConfiguration[config_num][1][agent_ID];

        // rotate agent's relative position to formation's direction
        float oriented_x_rel = cos(cons_param.theta_tilde) * x_rel - sin(cons_param.theta_tilde) * y_rel;
        float oriented_y_rel = sin(cons_param.theta_tilde) * x_rel + cos(cons_param.theta_tilde) * y_rel;

        cons_param.x_tilde = starting_pose.x + oriented_x_rel;
        cons_param.y_tilde = starting_pose.y + oriented_y_rel;
        cons_param.theta_tilde = starting_pose.theta;
        cons_param.v_tilde = getRand(30, 20);
    }

    // TODO: implement this in a new package containing libraries and definitions (e.g. LOOP_RATE and pi)
    float getRand(float max, float min = 0) {
        random_device rd; // non-deterministic 32-bit seed
        uniform_real_distribution<float> dist(min, max);
        return dist(rd);
    }

    void updateConsensus(const uni_msgs::ConsParamArray::ConstPtr &msg) {
        float x_sum = 0, y_sum = 0, v_sum = 0, theta_sum = 0;
        float n_params = msg->data.size();

        for (auto iterator : msg->data) {
            x_sum += iterator.x_tilde;
            y_sum += iterator.y_tilde;
            v_sum += iterator.v_tilde;
            theta_sum += iterator.theta_tilde;
        }

        cons_param.x_tilde = (cons_param.x_tilde + x_sum) / (1 + n_params);
        cons_param.y_tilde = (cons_param.y_tilde + y_sum) / (1 + n_params);
        cons_param.v_tilde = (cons_param.v_tilde + v_sum) / (1 + n_params);
        cons_param.theta_tilde = (cons_param.theta_tilde + theta_sum) / (1 + n_params);
    }

    // update reference output position, velocity and acceleration
    void updateReference() {
        float oriented_x_rel, oriented_y_rel;
        float dt = 1.0f / LOOP_RATE;

//        if (current_T == -1) return;

        // used to evaluate acceleration
        prev_vel.xd = ref_vel.xd;
        prev_vel.yd = ref_vel.yd;

        ref_vel.xd = cons_param.v_tilde * cos(cons_param.theta_tilde);
        ref_vel.yd = cons_param.v_tilde * sin(cons_param.theta_tilde);

        // rotate agent's relative position to formation's direction
        oriented_x_rel = cos(cons_param.theta_tilde) * x_rel - sin(cons_param.theta_tilde) * y_rel;
        oriented_y_rel = sin(cons_param.theta_tilde) * x_rel + cos(cons_param.theta_tilde) * y_rel;

        // update formation position
        cons_param.x_tilde += ref_vel.xd * dt;
        cons_param.y_tilde += ref_vel.yd * dt;

        ref_pos.x = cons_param.x_tilde + oriented_x_rel;
        ref_pos.y = cons_param.y_tilde + oriented_y_rel;

        // acceleration is not always zero, but neither is something algebraically derivable
        ref_acc.xdd = (ref_vel.xd - prev_vel.xd) / dt;
        ref_acc.ydd = (ref_vel.yd - prev_vel.yd) / dt;
    }

    void updateConfiguration(const std_msgs::Int32ConstPtr &msg) {
//        ROS_INFO("%d", msg->data);
        config_num = msg->data ? 1 : 0;

        x_rel = formationConfiguration[config_num][0][agent_ID];
        y_rel = formationConfiguration[config_num][1][agent_ID];
    }
};

//void updateTime(const std_msgs::Float64::ConstPtr &msg) {
//    current_T = static_cast<float>(msg->data);
//}

int main(int argc, char **argv) {
    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me agent ID!" << endl;
        return -1;
    }
    string agent_ID = argv[1];

    ros::init(argc, argv, "consensus_node"); // Initiate new ROS node named "consensus_node"

    ros::NodeHandle node;
    ros::Rate loop_rate(LOOP_RATE);

    waitForPose(agent_ID, &node, &loop_rate);
    Consensus cons(stoi(agent_ID));

    // reference signals publishers
    ros::Publisher pos_pub = node.advertise<uni_msgs::Pos>("pos_ref", 1);
    ros::Publisher vel_pub = node.advertise<uni_msgs::Vel>("vel_ref", 1);
    ros::Publisher acc_pub = node.advertise<uni_msgs::Acc>("acc_ref", 1);

    // consensus parameter publisher (to other agents, through consParamDistributor)
    ros::Publisher cons_pub = node.advertise<uni_msgs::ConsParam>("cons_param", 1);
    // and subscriber (to get parameters of other agents that are reachable from this agent)
    ros::Subscriber cons_sub = node.subscribe("others_param_list", 1,
                                              &Consensus::updateConsensus, &cons);

    ros::Subscriber conf_sub = node.subscribe("/form_conf", 1, &Consensus::updateConfiguration, &cons);

//    ros::Subscriber time_sub = node.subscribe("sim_time", 1, &updateTime);

    ROS_INFO("Consensus %s online", agent_ID.c_str());

    while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C

//        cons.updateConsensus();
        cons.updateReference();

        pos_pub.publish(cons.ref_pos);
        vel_pub.publish(cons.ref_vel);
        acc_pub.publish(cons.ref_acc);

        cons_pub.publish(cons.cons_param);

        ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
        loop_rate.sleep(); // Sleep for the rest of the cycle, to enforce the loop rate
    }
    return 0;
}