// Read formation consensus parameters (x, y, v, theta) form any agent and collect the correct
// list to forward to any other agent. Currently any agent communicate with all the others.

#include <ros/ros.h>

#include <uni_msgs/ConsParam.h>
#include <uni_msgs/ConsParamArray.h>
#include <std_msgs/Float32.h>

using namespace std;

const unsigned int LOOP_RATE = 100; // Hz

auto pi = 3.14142f;

// for a single agent
class ConsParamReader {
public:
    string agent_ID;

    // formation consensus parameters (x, y, v, theta)
    uni_msgs::ConsParam cons_param;

    ros::Publisher publisher;
    ros::Subscriber subscriber; // even if is never read, this variable must exist in order to let node subscriber to work properly

    // agent_0 can become a "leader" imposing its direction to others in the consensus algorithm
    bool activate_leader = false;
    ros::Subscriber theta_sub;
    float theta_des{};

    // to prevent actions before the first message
    bool has_messages = false;

    // initialize a subscriber (to get agent's parameter vector) and a publisher (to return other's parameter list)
    ConsParamReader(int id, ros::NodeHandle *node) {
        agent_ID = std::to_string(id);
        subscriber = node->subscribe("agent_" + agent_ID + "/cons_param", 1, &ConsParamReader::listener, this);
        publisher = node->advertise<uni_msgs::ConsParamArray>("agent_" + agent_ID + "/others_param_list", 1);

        if (id == 0) // if it's agent_0, then listen to teleop consensus parameters to alter consensus convergence
            theta_sub = node->subscribe("agent_0/theta_des", 1, &ConsParamReader::thetaUpdate, this);
    }

    // listener available only by agent_0
    void thetaUpdate(const std_msgs::Float32ConstPtr &msg) {
        activate_leader = true;
        theta_des = msg->data;
    }

    // callback for topic "cons_param_<j>"
    void listener(const uni_msgs::ConsParam::ConstPtr &msg) {
        has_messages = true;

        cons_param.x_tilde = msg->x_tilde;
        cons_param.y_tilde = msg->y_tilde;
        cons_param.v_tilde = msg->v_tilde;
        cons_param.theta_tilde = activate_leader ? theta_des : msg->theta_tilde;
    }
};

int main(int argc, char **argv) {
    uni_msgs::ConsParamArray reached_param_list;

    // check number of input arguments
    if (argc == 1) {
        cout << "Invalid call: you've to tell me how many agents!" << endl;
        return -1;
    }
    int n_agents = stoi(argv[1]);

    ros::init(argc, argv, "consParamDistributor_node");
    ros::NodeHandle node;
    ros::Rate loop_rate(LOOP_RATE);

    // initialize readers (one for each agent)
    ConsParamReader *agents[n_agents];
    for (int j = 0; j < n_agents; j++)
        agents[j] = new ConsParamReader(j, &node);

    ROS_INFO("consParamDistributor online");

    while (ros::ok()) {

        /// give to every agent the list of other agent's parameters ///
        for (int j = 0; j < n_agents; j++) {
            reached_param_list.data.clear();

            for (int i = 0; i < n_agents; i++)
                if (i != j && agents[i]->has_messages)
                    reached_param_list.data.push_back(agents[i]->cons_param);

            agents[j]->publisher.publish(reached_param_list);
        }

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}