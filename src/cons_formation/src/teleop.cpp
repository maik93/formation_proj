#include <ros/ros.h>
#include <ncurses.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <uni_msgs/ConsParam.h>

const float DELTA_THETA = 3.14142f / 32.0f;

// non blocking check for key pressed
int kbhit() {
    int ch = getch();

    if (ch != ERR) {
        ungetch(ch); // put that key again in queue, so I can read it later
        return 1;
    } else {
        return 0;
    }
}

int main(int argc, char **argv) {
    std_msgs::Int32 conf;
    std_msgs::Float32 theta_des;

    theta_des.data = 0;
    int input_char = 0;
    bool arrow_form = true;

    /// ROS stuff ///
    ros::init(argc, argv, "teleop_node");
    ros::NodeHandle node;
    ros::Rate loop_rate(30); // Hz

    ros::Publisher conf_pub = node.advertise<std_msgs::Int32>("/form_conf", 1);

    ros::Publisher theta_pub = node.advertise<std_msgs::Float32>("agent_0/theta_des", 1); // to consParamDistributor

    /// start Curses ///
    initscr();
    raw();
    noecho();
    nodelay(stdscr, TRUE);
    scrollok(stdscr, TRUE);

    while (ros::ok() && input_char != 27) { // Esc key

        /// Graphics and key listener ///
        clear();
        if (kbhit()) {
            input_char = getch();
            switch (input_char) {
                case 10: // Enter key
                    arrow_form = !arrow_form;

                    // publish the update
                    conf.data = arrow_form ? 0 : 1;
                    conf_pub.publish(conf);
                    break;
                case 160: // 'à' key
                    theta_des.data -= DELTA_THETA;
                    break;
                case 185: // 'ù' key
                    theta_des.data += DELTA_THETA;
                    break;
                default:
                    break;
            }
        }

        theta_pub.publish(theta_des);

        printw("Formation type: ");
        attron(A_BOLD);
        if (arrow_form) printw("arrow\n");
        else printw("row\n");
        attroff(A_BOLD);
        printw("Press enter to change.\n\n");

        printw("Use 'à' and 'ù' to regulate theta.\nCurrent theta: ");
        attron(A_BOLD);
        printw("%.2f", theta_des.data);
        attroff(A_BOLD);

//        printw("%d", input_char); // show keycodes
        refresh();

        ros::spinOnce();
        loop_rate.sleep();
    }
    endwin();

    return 0;
}