#ifndef PROJECT_CONS_GLOBAL_VARS_H
#define PROJECT_CONS_GLOBAL_VARS_H

// equilateral triangle
//const float formationConfiguration[2][3] = {{40, -20,  -20},
//                                            {0,  35, -35}};

// arrow formation
//const float formationConfiguration[2][5] = {{20, -8, -8,  -36, -36},
//                                            {0,  29, -28, 57,  -56}};

// arrow formation & line formation
const float formationConfiguration[2][2][5] = {{{20, -18, -18, -57, -57},
                                                       {0, 38, -38, 78, -77}},
                                               {{0,  0,   0,   0,   0},
                                                       {0, 40, -40, 80, -80}}};

#endif //PROJECT_CONS_GLOBAL_VARS_H
